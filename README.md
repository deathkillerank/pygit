# PY-GIT
pygit is a small implementation of a Git-like version control system (VCS) in python. It's top goal is simplicity and educational value. 


## Directory & File Structure

### pygit
#### cli.py - In charge of parsing and processing user input.
#### data.py - Manages the data in .pygit directory. Here will be the code that actually touches files on disk.